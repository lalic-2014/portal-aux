package PorTAl::Aux;

use 5.012004;
use strict;
use locale;
use warnings;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use PorTAl::Aux ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(zera_arquivo le_sentencas le_sentencas_cruas imprime_fim imprime_cabecalho imprime_sentenca_alinhada imprime_sentenca_etiquetada imprime_sentenca_etiquetada_alinhada imprime_sentenca_limpa compara aviso separa_tokens erro remove_etiquetas
	
);

our $VERSION = '12.09';


# Preloaded methods go here.
#****************************************************************************************
#                                PROCESSAMENTO DE TOKENS
#****************************************************************************************
# Subrotina separa_tokens
# Entrada: $sentenca (string)
# Saida: Uma string com o conteudo de $sentenca, mas com os espacos separando tokens
# Funcao: Insere espacos entre os tokens da string $sentenca. Tokens sao palavras, 
# numeros, caracteres de pontuacao e contracoes do ingles como 's ou 've.
# Exemplo: 
# Entrada: $str = 'Esse artigo trata do problema de 1.2% dos casos.'
# Saida: $str = 'Esse artigo trata do problema de 1.2 % dos casos .'
sub separa_tokens {
	my($sentenca) = @_;

	if ($sentenca =~ /^\s+$/) { return ''; }          
    
	$sentenca =~ s/“/\"/g;		# Substitui aspas especificas por aspas duplas
	$sentenca =~ s/”/\"/g;		# Substitui aspas especificas por aspas duplas
	$sentenca =~ s/\'\'/\"/g;	# Substitui sequencia de apostrofes seguidos por aspas duplas
	$sentenca =~ s/``/\"/g;		# Substitui sequencia de apostrofes seguidos por aspas duplas

   # Inserindo espaco nos lugares adequados para fazer a separacao por ele depois - Inicio
	# Para conseguir usar o etiquetador tive que processar separadamente alguns caracteres   
   $sentenca =~ s/([\\\/\"\!\(\)\{\}\[\]\?\:\;\@\%\&\=\+\~\>\<])/ $1 /g;    
	$sentenca =~ s/¡/ ¡ /g; 								
   $sentenca =~ s/¿/ ¿ /g; 				
   $sentenca =~ s/º/ º /g; 				
   $sentenca =~ s/°/ ° /g; 				
   $sentenca =~ s/ª/ ª /g; 				

   # Insere espaco antes e depois do $ senao etiquetador da erro
   $sentenca =~ s/\$/ \$ /g;

   # INICIO - Trata . e , de maneira especial    
   $sentenca =~ s/\.\.\./ . . . /g;        
   $sentenca =~ s/([0-9])([\.\,])([0-9])/$1_$2_$3/g;
   $sentenca =~ s/([^_])([\,\.])/$1 $2 /g;
   $sentenca =~ s/_([\.\,])_/$1/g;    
   # FIM - Trata . e ,  de maneira especial
            
   # INICIO - Trata 's presente nos textos em ingles como um unico token  
   $sentenca =~ s/^([\'|\`])/ $1 /g;
   $sentenca =~ s/ ([\'|\`])/ $1 /g; 
   $sentenca =~ s/ ([\'|\`])/ $1 /g; 	# Trata casos '' ou `` no inicio da sentenca
   $sentenca =~ s/([\'|\`]) / $1 /g;
   $sentenca =~ s/([\'|\`]) / $1 /g; 	# Trata casos '' ou `` no fim da sentenca
	# Daqui para baixo substituo ` por ' para que o etiquetador consiga tratar
	$sentenca =~ s/([^ ])\`/$1'/g; 		# Decidi nao separar o ' para o etiquetador tratar adequadamente
   # FIM - Trata 's presente nos textos em ingles como um unico token  

   $sentenca =~ s/\s+/ /g; 	# Remove o excesso de espacos
   $sentenca =~ s/^\s+//g; 	# Remove espacos no comeco da sentenca 

   return $sentenca;    
}

#****************************************************************************************
#                                ENTRADA
#****************************************************************************************
sub le_sentencas_cruas {
	my($arq,$hashsent,$separa) = @_;
	
	my($idsent);
	
	$idsent = 1;
	open(FILE,$arq) or erro(2,$arq);
	while (<FILE>) {
		if ($_ =~ /\w/) {
			$_ =~ s/[\n\r]//g;			
      if ($separa) { 
        $$hashsent{$idsent++} = separa_tokens($_); 
      }
			else { $$hashsent{$idsent++} = $_; }
		}		
	}
	close FILE;	
}

# Subrotina le_sentencas
# Entrada: $arq (caminho para um arquivo)
#          $hash (hash no qual as sentencas serao salvas)
#          $separa (se 1 separa tokens, 0 nao separa)
# Saida: $hash e $hashal (se houver) alterados com as sentencas lidas do arquivo $arq e seus alinhamentos, respectivamente
# Funcao: Armazena cada as sentencas do arquivo $arq em um hash sendo a chave o identificador da sentenca
# Armazena tambem os alinhamentos em $hashal, se a sentenca estiver alinhada sentencialmente
# IMPORTANTE: A sentenca de entrada esta
# 	- no minimo delimitada por fronteiras (com tags <text>, <p> e <s>) 
#		=> Entrada para Alinhador de Sentencas e Etiquetador Morfossintatico
#	- pode estar alinhada sentencialmente (com atrs id crr e atype em <s>)  
#		=> Entrada para Etiquetador Morfossintatico e Alinhador de Palavras
#	- pode estar etiquetada morfossintaticamente (com tags <w> e </w> dentre de <s> e </s>)  
#		=> Entrada para Alinhador de Palavras
# ESTRUTURA DE DADOS (SAIDA):
# 	$hash 	- um hash com chave = id da sentenca e valor = texto da sentenca
# 	$hashal 	- um hash com chave = id da sentenca "fonte" e valor = array com ids das sentencas "alvo"
# Exemplo (saida):
#  $hash{art1P.1.s1}{'sentenca'} = 'Esse artigo trata do problema de . . .'      
#  $hashal{art1P.1.s1} = 'art1E.1.s1'
sub le_sentencas {
	my($arq,$hash,$separa) = @_;
	my(@linhas,@aux,$sentenca,$id,$corresp,$tipo,$cab,$nome,$contp,$conts);
  
	open(ARQ,"$arq") or erro(2,$arq);
	@linhas = <ARQ>;
	close ARQ;

	$conts = $contp = 0;	
	map(s/\n//,@linhas); #/
	if ($linhas[0] =~ /<text [^>]*id=\"([^\"]+)\"[^>]*>/) { 
		$nome = $1;
		$cab = shift(@linhas); 
	}
	while ($#linhas >= 0) {    
		if ($linhas[0] =~ /<s id=/) { # Sentenca esta alinhada sentencialmente
			@aux = grep(/crr/,split(/<s /,$linhas[0]));
			while ($#aux >= 0) {           
				$_ = shift(@aux);
				# <s id="teste-pt.1.s1" crr="teste-en.1.s1" atype="t11">Os dentes do mais antigo orangotango</s>

				if (($id,$corresp,$tipo,$sentenca) = /id=\"([^\"]+)\"\s*crr=\"([^\"]*)\"\s*atype=\"([^\"]+)\"[^>]*>(.+)<\/s>/) {
  					if (($separa) && ($sentenca !~ /<w id=/)) { 
		    	        $$hash{$id}{'sentenca'} = separa_tokens($sentenca); 
    				}
  					else { $$hash{$id}{'sentenca'} = $sentenca; }
  					$$hash{$id}{'crr'} = $corresp;		
  					$$hash{$id}{'atype'} = $tipo;			
  				}
				else { erro(3,$linhas[0]); }
			}
		}  
		else { # Sentenca esta apenas delimitada por fronteiras
			if ($linhas[0] =~ /<p>/) { 
				$contp++; 
				$conts = 0;
			}
			if ($linhas[0] =~ /<s>/) { 
				@aux = grep(/\w/,split(/<\/s>/,$linhas[0]));
				while ($#aux >= 0) {
					$sentenca = shift(@aux);
					$sentenca =~ s/<[s|p]>//g;
					$conts++; 
					$id = $nome.'.'.$contp.'.'.$conts;  
          if (($separa) && ($sentenca !~ /<w id=/)) { 
            $$hash{$id}{'sentenca'} = separa_tokens($sentenca); 
          }
  				else { $$hash{$id}{'sentenca'} = $sentenca; }
				}
			}
			elsif (($linhas[0] !~ /<p>/) && ($linhas[0] !~ /<\/p>/) && ($linhas[0] !~ /<\/text>/)) { 
				erro(3,$linhas[0]); 
			}
		}
		shift(@linhas);
	}
	return $cab;
}

#****************************************************************************************
#                                         SAIDA
#****************************************************************************************
sub imprime_cabecalho {
	my($arq,$cab) = @_;
	
	open(ARQ,">>$arq") or erro(2,$arq);
	print ARQ "$cab\n";
	close ARQ;
}

sub imprime_fim {
	my($arq) = @_;
	
	open(ARQ,">>$arq") or erro(2,$arq);
	print ARQ "<\/p>\n<\/text>\n"; 
	close ARQ;
}

sub imprime_sentenca_etiquetada {
	my($arq,$cabs,$sentenca,$cont) = @_;
  my($cabw,$word);

	open(OUT,">>$arq") or erro(2,$arq);
	$cabs =~ /id=\"[^\.]+\.(\d+)\.(\d+)\"/;
	if ($2 == 1) {
		if ($1 != 1) { print OUT "<\/p>\n"; }
		print OUT "<p>\n"; 
	}
	if ($cabs =~ /crr=\"/) {
		print OUT $cabs;
	}
	else { print OUT "<s>"; }
	my @tokens = grep(length($_) > 0,split(/ +/,$sentenca));
  while ($#tokens >= 0) {
    if ($tokens[0] =~ /(<w-_-id=[^>]+)>(.+)/) {
      $cabw = $1;
      $word = $2;  
      $cabw =~ s/-_-/ /g;
    }
    else {
      $cabw = "<w id=\"".($$cont++)."\"";
      $word = $tokens[0];
    }

    print OUT $cabw." ".formata_etiquetado(\$word).">".$word."</w> ";
    shift(@tokens);
  }
#  print OUT join(" ",map("<w id=\"".($$cont++)."\" ".formata_etiquetado(\$_).">".$_."</w>",@tokens));
	print OUT "</s>","\n";
	close OUT;
}


sub imprime_sentenca_alinhada {
	my($arq,$hash,$fontes,$alvos,$op,$idali,$tipo) = @_;
	my($corresp,$i,$aux);

	if ($op) {
		$corresp = "";
		if ($#$alvos > 0) { $corresp = join(",",map($_,@$alvos)); }
		elsif ($#$alvos == 0) { $corresp = $$alvos[0]; }
	}
	
	$i = 0;
	open(OUT,">>$arq") or erro(2,$arq);
	while ($i <= $#$fontes) {
		if ($op) { # imprime com id e corresp
			$$fontes[$i] =~ /[^\.]+\.(\d+)\.(\d+)/;
			if ($2 == 1) {
				if ($1 != 1) { print OUT "<\/p>\n"; }
				print OUT "<p>\n"; 
			}
			print OUT "<s id=\"$$fontes[$i]\" crr=\"$corresp\" atype=\"$tipo\">",$$hash{$$fontes[$i++]}{'sentenca'},"<\/s>\n";
		}		
		else { # imprime com snum
			print OUT "<s snum=",$idali,">",$$hash{$$fontes[$i++]}{'sentenca'},"<\/s>\n";
		}		
	}	
	close OUT;	
}

sub imprime_sentenca_etiquetada_alinhada {
	my($arq,$cabs,$arraysent) = @_;
	my $sentenca = join(" ",map(formata_alinhado($_),@$arraysent));

	open(OUT,">>$arq") or erro(2,$arq);
	$cabs =~ /id=\"[^\.]+\.(\d+)\.(\d+)\"/;
	if ($2 == 1) {
		if ($1 != 1) { print OUT "<\/p>\n"; }
		print OUT "<p>\n"; 
	}
	print OUT $cabs,$sentenca,"</s>","\n";
	close OUT;
}

sub imprime_sentenca_limpa {
	my($arq,$sentenca) = @_;

	remove_etiquetas(\$sentenca);
	open(OUT,">>$arq") or erro(2,$arq);
	print OUT $sentenca,"\n";
	close OUT;
}



#****************************************************************************************
#                                SAIDA - FORMATACAO
#****************************************************************************************
# Sub formata_etiquetado
# Entrada: $token (token etiquetado morfossintaticamente)
# Saida: $$token alterado para conter apenas a forma superficial da palavra e String com o token formatado no formato do PorTAl
# Funcao: Formata o token etiquetado morfossintaticamente para o formalismo do PorTAl
sub formata_etiquetado {
	my($token) = @_;
	my($sup,$lema,$etiquetas,$atributos,@aux,$t,$et,$atr);

	$t = $$token;
	if ($t =~ /^(.+)\/(.+)$/) { # Verifica se o token foi etiquetado
		$sup = $1; $t = $2;		# separando-o em forma superficial ($sup) e resto ($t)
        if ($t =~/^<.+>$/) {    # So tem etiquetas, nenhum lema
            $t = $sup.$t;           # Considera como lema a propria forma superficial
        }
	}
	else { $sup = $t; }	# Se ele nao foi etiquetado so existe a forma superficial ($sup)

	# Separa contracoes do tipo: das 
	# das/de<pr>+o<det><def><f><pl>
	if ($sup ne '+') {	
		$t =~ s/\>_/>+_/g;
		@aux = split(/\+/,$t);
	}
	else { @aux = ($t); }

	$lema = $etiquetas = $atributos = "";

	while ($#aux >= 0) {
		$_ = shift(@aux);
		$t = $et = $atr = '';
		if (/^\*(.+)$/) { $t = $1; }			# palavra desconhecida
		elsif (/^([^<]+)$/) { $t = $1; }		# token nao etiquetado
		elsif (/^([^<]*)\<([^>]+)\>(\<.+\>)*(.*)\*([^<]*)$/) { ($t,$et,$atr) = ($1.$4.$5,$2,$3); }		
		elsif (/^([^<]*)\<([^>]+)\>(\<.+\>)*([^<]*)$/) { ($t,$et,$atr) = ($1.$4,$2,$3); }
		elsif (/^([^<]*)\<([^>]+)\>(\<.+\>)*(.*)\*(.+)$/) { 
			($t,$et,$atr) = ($1,$2,$3); 
			$t .= $4.$5;
		}
		$t = ($t ne '') ? $t : 'NC';
		$et = ($et ne '') ? $et : 'NC';
		$atr = (defined($atr) && ($atr ne '')) ? $atr : 'NC';
		$etiquetas .= ($etiquetas eq '') ? $et : '+'.$et;
		$atributos .= ($atributos eq '') ? $atr : '+'.$atr;
		$atributos =~ s/<//g;
		$atributos =~ s/>/|/g;
		$atributos =~ s/\|$//;
		$lema .= ($lema eq '') ? $t : '+'.$t;
	}
    $sup =~ s/-barra-/\//g;
    $lema =~ s/-barra-/\//g;
	$$token = $sup;
	if (($lema eq '"') || ($lema eq '<') || ($lema eq '>')) { # Poe o lema entre aspas simples e nao duplas
		return "lemma='".$lema."' pos=\"".mapeia_pos($etiquetas)."\"".formata_atributos($atributos); 
	}
	return "lemma=\"".$lema."\" pos=\"".mapeia_pos($etiquetas)."\"".formata_atributos($atributos); 
}

# Substitui algumas PoS retornadas pelo etiquetador para padrao do PorTAl
# detnt => det
# vblex, vbhaver, vbdo, vbmod, vbser => v
# infps => inf
#
sub mapeia_pos {
 my($pos) = @_;
 
 $pos =~ s/detnt/det/g;
 $pos =~ s/vb[^\+]+/v/g;
 $pos =~ s/infps/inf/g;  
 return $pos;
}

# Substitui alguns atributos retornados pelo etiquetador para padrao do PorTAl
# cni => ind|futpret
# fti => ind|futpres
# pii => ind|pretimp
# pmp => ind|pretmqp
# ifi => ind|pretperf
# pri => ind|pres
# prs => subj|pres
# pis => subj|pretimp
# fts => subj|fut
sub mapeia_atributo {
 my($atr) = @_;
 
 $atr =~ s/cni/indi\|futpret/;
 $atr =~ s/fti/indi\|futpres/;
 $atr =~ s/pii/indi\|pretimp/;
 $atr =~ s/pmp/indi\|pretmqp/;
 $atr =~ s/ifi/indi\|pretperf/;
 $atr =~ s/pri/indi\|pres/;
 $atr =~ s/prs/subju\|pres/;
 $atr =~ s/pis/subju\|pretimp/;
 $atr =~ s/fts/subju\|fut/;
 return $atr;
}

# 7 valores cadastrados em tipo_atr_palavra:
#  dic_categoria
#  dic_genero
#  dic_numero
#  dic_tempo_verbal
#  dic_forma_verbal
#  dic_pessoa
#  dic_tipo
# Com excecao do primeiro, cujo valor esta em pos, os outros 6 tipos darao origem a ate 6 atributos de uma palavra
# Valores que cada um dos 7 tipos pode assumir
#  dic_categoria: adj, adv, cnjadv, cnjcoo, cnjsub, det, ij, n, np, num, pr, prn, rel, v, vaux (e outros)
#  dic_genero: f, m, nt, mf
#  dic_numero: sg, pl, sp
#  dic_tempo_verbal: pres, past, fut, pretimp, pretperf, pretmqp, futpres, futpret
#  dic_forma_verbal: inf, pp, ger, ind, subj, imp
#  dic_pessoa: p1, p2, p3
#  dic_tipo: outros valores se enquadram como tipo
sub separa_atributos {
	my($atr,$valores,$id) = @_;
	my $tipo = "";
	
	if (($atr eq "f") || ($atr eq "m") || ($atr eq "nt") || ($atr eq "mf")) { # Genero
		$tipo = "gender";
	}
	elsif (($atr eq "sg") || ($atr eq "pl") || ($atr eq "sp")) { # Numero
		$tipo = "number";
 	}
 	elsif (($atr eq "pres") || ($atr eq "past") || ($atr eq "fut") || ($atr eq "pretimp") || 
        ($atr eq "pretperf") || ($atr eq "pretmqp") || ($atr eq "futpres") || ($atr eq "futpret")) { # Tempo verbal
  		$tipo = "time";
 	}
 	elsif (($atr eq "inf") || ($atr eq "pp") || ($atr eq "ger") || ($atr eq "indi") || 
        ($atr eq "subju") || ($atr eq "imp")) { # Forma verbal
  		$atr =~ s/indi/ind/;      
  		$atr =~ s/subju/subj/;   
		$tipo = "form";
 	}
 	elsif ($atr =~ /p\d/) { # Pessoa
  		$tipo = "person"; 
 	}
 	else { $tipo = "type"; }
 	
	if (($tipo ne "") && (pertence($atr,\@{$$valores{$tipo}{$id}}) == 0)) { 
		push(@{$$valores{$tipo}{$id}},$atr); 
	}
}

sub pertence {
	my($elemento,$array) = @_;
	my(%aux) = map(($_,0),@$array);
	return defined($aux{$elemento});
}

sub formata_atributos {
	my($atributos) = @_;
	my(@tokens,@chaves,$cont,$i,@atrs);
 	my %valores = ();

	if ($atributos !~ /\w/) { return ""; }

	$atributos = mapeia_atributo($atributos);
	if ($atributos =~ /\+/) {
		@tokens = split(/\+/,$atributos);
	}
	else { @tokens = ($atributos); }

	$atributos = "";
	
	$cont = 0;
	while ($#tokens >= 0) { # Para cada parte do token sendo processado
		@atrs = grep($_ ne "NC",split(/\|/,shift(@tokens)));
		if ($#atrs >= 0) {
			map(separa_atributos($_,\%valores,$cont),@atrs);
		}
		$cont++;
 	}
 	
	@chaves = sort keys %valores;
	$atributos = " ";
	while ($#chaves >= 0) {
		$atributos .= $chaves[0]."=\"".join("+",map(defined($valores{$chaves[0]}{$_}) ? join(",",@{$valores{$chaves[0]}{$_}}) : "NC",0..$cont-1))."\" ";
		shift(@chaves);
	}
	$atributos =~ s/ $//;

 	return $atributos;
}


# Sub formata_alinhado
# Entrada: $token (token alinhado lexicalmente)
# Saida: String com o token formatado no formato do PorTAl
# Funcao: Formata o token alinhado lexicalmente para o formalismo do PorTAl
sub formata_alinhado {
	my($token) = @_;
	my($t,$al,$atype,$tags);

	if ($token =~ /^(.+):(.+)\|(.+)$/) {
		$t = $1;	# token possivelmente com tags morfossintaticas delimitado entre <w> e </w>
		$al = $2;
		$atype = $3;
		$al =~ s/_/,/g;
		$al =~ s/^0$//g;
		$atype =~ s/-/:/;
		if ($atype eq "01") { $atype = "0:1"; }
	}
	else { # Nao foi alinhado corretamente
		$t = $token;
		$al = "";
		$atype = "1:0";
	}
	$atype = 't'.$atype;
	$t =~ s/^<([^>]+)>(.+)$/<$1 crr=\"$al\" atype=\"$atype\">$2/;
	return $t;
}

#****************************************************************************************
#                                ORDENACAO
#****************************************************************************************
# Retorna -1 se $a for menor que $b e 1 caso contrario
sub compara {
	my($a,$b) = @_;
	my($sa,$sb,$pa,$pb);

	$a =~ /[^\.]+\.(\d+)\.(\d+)/;
	$pa = int($1);
	$sa = int($2);
	$b =~ /[^\.]+\.(\d+)\.(\d+)/;
	$pb = int($1);
	$sb = int($2);
	if ($pa < $pb) { return -1; }
	elsif ($pa == $pb) {
		if ($sa < $sb) { return -1; }
	}
	return 1;
}

#****************************************************************************************
#                                INICIALIZACAO e LIMPEZA
#****************************************************************************************
# Zera (inicializa) um arquivo
sub zera_arquivo {
	my($arq) = @_;
	
	if (open(ARQ,">$arq") != 1) { 
		erro(2,$arq);
	}
	close ARQ;
}

sub remove_etiquetas {
	my($sent) = @_;

	$$sent =~ s/<\/*text[^>]*>//;
	$$sent =~ s/<\/*p>//;
	$$sent =~ s/<s[^>]*>(.+)<\/s>/$1/;
	my @tokens = ();
	if ($$sent =~ /<w/) { 
		@tokens = split(/<\/w>/,$$sent); 
    map(s/lemma='>'//,@tokens); #/ #Para nao atrapalhar a expressao regular a seguir
		map(s/ *<w[^>]+>//,@tokens);
		$$sent = join(" ",@tokens);
	}
}


#****************************************************************************************
#                                INTERFACE - ERRO
#****************************************************************************************
sub erro {
	my($tipo,$msg) = @_;

	if ($tipo == 2) { 
		print STDERR "ERRO $tipo: Nao eh possivel abrir o arquivo $msg\n";
#		print STDERR "ERRO $tipo: It is not possible to open file $msg\n";
	}
	elsif ($tipo == 3) {
		print STDERR "ERRO $tipo: Sentenca com formato nao permitido $msg\n";
	}
	else {
		print STDERR "ERRO $tipo: $msg\n";
	}
	exit $tipo;
}

sub aviso {
	my($tipo,$ferramenta,$msg) = @_;

	if (($tipo == 1) && ($ferramenta == 3)) { 
		print STDERR "AVISO $tipo (Etiquetador_Morfossintatico): ERRO na etiquetacao da sentenca $msg\n";
	}
	else {
		print STDERR "AVISO $tipo ($ferramenta)\n";
	}
}


1;
__END__

=head1 NAME

PorTAl::Aux - Perl extension for Auxiliary modules for PorTAl

=head1 SYNOPSIS

  use PorTAl::Aux;
 
=head1 DESCRIPTION

Auxiliary methods for PorTAl tools

=head2 EXPORT

None by default.



=head1 SEE ALSO



=head1 AUTHOR

LaLiC, www.lalic.dc.ufscar.br

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010-2012 by LaLiC

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.12.4 or,
at your option, any later version of Perl 5 you may have available.


=cut
